# Pre-Install
- composer
- php
- nodejs
- yarn
- git
- sublime merge
- phpstorm on toolbox
- MySQL

Make sure MySQL is set up on your machine. You will know if it is not set up if
you get a PDOexception error when running `php artisan migrate`. It is necessary
that you create a database (schema) with the name `homestead` as that is the
database name set up in the .env file. Also use the username `admin` and
password `password`.

# Install

- `git clone git@gitlab.com:seth.ballance/laravel-site.git`
- `cp .env.example .env` Linux. `copy .env.example .env` Windows.
- `composer install` dev. `composer install --no-dev` production. 
- `php artisan key:generate`
- `php artisan migrate`

# SSH Setup

Follow Gitlab's SSH setup instructions:
[SSH Keys](https://gitlab.com/profile/keys)

If you already have an SSH key on the machine just reuse it. If you don't it
causes lots of problems.

# Startup
To start the server for this site use the following command
```
composer start
```

This will start up a server on `http://localhost:8000`

# Branch Change
When switching branches, be sure to run `setup_branch.bat`.
This will run scripts to build up the js and css files and update any packages.

# Image hosting
I was originally planning on hosting project images on google photos but it
looks like they won't let me so I am using [imggmi](https://imggmi.com/).
