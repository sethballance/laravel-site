module.exports = function({ addUtilities, e, theme, variants }) {
    const gradients = theme('gradients', {});
    const gradientVariants = variants('gradients', []);

    const utilities = Object.keys(gradients).map(gradient => ({
        [`.bg-horizontal-gradient-${gradient}`]: {
            backgroundImage: `linear-gradient(to right, ${gradients[gradient][0]}, ${gradients[gradient][1]})`
        },
        [`.bg-vertical-gradient-${gradient}`]: {
            backgroundImage: `linear-gradient(to bottom, ${gradients[gradient][0]}, ${gradients[gradient][1]})`
        }
    }))

    addUtilities(utilities, gradientVariants)
}
