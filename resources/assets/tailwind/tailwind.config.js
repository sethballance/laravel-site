module.exports = {
    theme: {
        extend: {
            colors: {
                gray: {
                    50: '#fafafa',
                    100: '#f5f5f5',
                    200: '#eeeeee',
                    300: '#e0e0e0',
                    400: '#bdbdbd',
                    500: '#9e9e9e',
                    600: '#757575',
                    700: '#616161',
                    800: '#424242',
                    900: '#212121',
                }
            },
            fontFamily: {
                mono: [
                    'Orbitron',
                    'Menlo',
                    'Monaco',
                    'Consolas',
                    '"Liberation Mono"',
                    '"Courier New"',
                    'monospace',
                ],
            },
            inset: (theme, { negative }) => ({
                ...theme('spacing'),
                ...negative(theme('spacing'))
            }),
            container: {
                center: true,
            },
            spacing: {
                '2px': '2px',
                full: '100%'
            },
            gradients: theme => ({
                'transparent-white': [theme('colors.transparent'), theme('colors.white')],
                'gray-900-gray-600': [theme('colors.gray.900'), theme('colors.gray.600')]
            })
        },
    },
    variants: {
        borderWidth: ['responsive', 'hover', 'focus', 'first', 'last'],
        borderRadius: ['responsive', 'hover', 'focus', 'first', 'last'],
        margin: ['responsive', 'hover', 'focus', 'first', 'last'],
        backgroundColor: ['responsive', 'hover', 'focus', 'odd'],
        gradients: ['responsive', 'hover']
    },
    plugins: [
        require('./gradients')
    ]
}
