import VueRouter from 'vue-router';
import Welcome from './components/views/welcome';
import About from './components/views/about';
import Projects from './components/views/projects/index';
import Project from './components/views/projects/show';
import createProject from './components/views/projects/create';
import editProject from './components/views/projects/edit';

import getToken from './components/views/auth/get';
import Login from './components/views/auth/login';

import {getCookie} from './cookie';

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Welcome
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/token',
            name: 'token',
            component: getToken,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/projects',
            name: 'projects',
            component: Projects
        },
        {
            path: '/projects/create',
            name: 'createProject',
            component: createProject,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/projects/:project_id',
            name: 'project',
            component: Project
        },
        {
            path: '/projects/:project_id/edit',
            name: 'editProject',
            component: editProject,
            meta: {
                requiresAuth: true
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!isSignedIn()) {
            next({
                name: 'login'
            });
        } else {
            next();
        }
    } else {
      next();
    }
});

function isSignedIn() {
    return getCookie('authToken');
}

export default router;
