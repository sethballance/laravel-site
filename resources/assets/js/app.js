import Vue from "vue";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import router from './routes';
import VueRouter from 'vue-router';
import {getCookie} from './cookie';

window.Vue = require('vue');

Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('container', require('./components/container'));
Vue.component('navbar', require('./components/navbar'));

const app = new Vue({
    el: '#app',
    router,

    data() {
        return {
            isSignedIn: false
        }
    },
    created() {
        this.updateSignedIn();
    },
    methods: {
        updateSignedIn() {
            this.isSignedIn = getCookie('authToken') ? true : false;
        }
    }
});
