@ECHO OFF

REM Put site in maintenance mode
php artisan down

REM Clear Compiled Classes
php artisan config:clear
php artisan route:clear
php artisan view:clear
php artisan clear-compiled

REM Update git
git fetch -p
git pull

REM Install composer packages
call composer install

REM Clear cache
php artisan cache:clear

REM Compile css and js
call yarn install
call yarn dev

REM Bring site out of maintenance mode
php artisan up
