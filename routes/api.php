<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('auth/token', 'AuthTokenController@get')->name('api.auth.get');

Route::post('auth/token', 'AuthTokenController@login')->name('api.auth.login');

Route::group(['prefix' => 'auth'], function () {
    Route::get('token', 'AuthTokenController@get')->name('api.auth.get');
    Route::post('token', 'AuthTokenController@login')->name('api.auth.login');
});

Route::group(['prefix' => 'project'], function () {
    Route::get('getAll', 'ProjectController@getAll')->name('project.getAll');
    Route::get('{projectId}', 'ProjectController@get')->name('project.get');
    Route::post('/', 'ProjectController@store')->name('project.store');
    Route::put('{projectId}', 'ProjectController@update')->name('project.update');
    Route::post('/image/{projectId}', 'ProjectController@image')->name('project.image');
});

Route::group(['prefix' => 'tag'], function () {
    Route::get('getAll', 'TagController@getAll')->name('tag.getAll');
});
