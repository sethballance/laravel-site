<?php

namespace App\Http\Controllers\Api;

use App\Tag;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    public function getAll()
    {
        $tags = Tag::get();

        return $tags;
    }
}
