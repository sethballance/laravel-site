<?php

namespace App\Http\Controllers\Api;

use App\AuthToken;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class AuthTokenController extends Controller
{
    public function get()
    {
        $token = AuthToken::first();

        return $token->hash;
    }

    /*
    Only uses the first token
    */
    public function login(Request $request)
    {
        $hash = $request->input('tokenHash');

        if ($hash == AuthToken::first()->hash) {
            $response = new Response('Authenticating');
            $response->withCookie(cookie()->forever('authToken', $hash, null, null, null, false));

            return $response;
        } else {
            return response()->json(['error' => 'Invalid token.'], 401);
        }
    }
}
