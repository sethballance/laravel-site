<?php

namespace App\Http\Controllers\Api;

use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    public function getAll()
    {
        $projects = Project::with('tags')->get();

        return $projects;
    }

    public function get($projectId)
    {
        $project = Project::with('tags')->find($projectId);

        return $project;
    }

    public function store(Request $request)
    {
        $this->validateProject($request);

        $project = Project::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'project_link' => $request->input('project_link')
        ]);

        $project->tags()->attach($request->input('tags'));

        return response()->json($project->id);
    }

    public function update($projectId, Request $request)
    {
        $this->validateProject($request, $projectId);

        $project = Project::find($projectId);

        $project->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'project_link' => $request->input('project_link')
        ]);

        $project->tags()->detach();

        $project->tags()->attach($request->input('tags'));

        return response()->json('success', 200);
    }

    public function image($projectId, Request $request)
    {
        $request->validate([
            'project_image' => 'required|image|mimes:jpeg,jpg,png'
        ]);
        $image = $request->file('project_image');
        $destinationPath = public_path('/images/projects');
        Image::make($image)->fit(1200, 800)->save("$destinationPath/$projectId.jpg");
    }

    private function validateProject(Request $request, $projectId = null)
    {
        return $request->validate([
            'title' => 'required|string|max:255|unique:projects,title,'.$projectId,
            'description' => 'required|string|max:2000',
            'project_link' => 'nullable|active_url',
            'tags' => 'array'
        ]);
    }
}
