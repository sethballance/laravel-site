<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class AuthToken extends Model
{
    protected $table = 'authorization_token';

    public static function reset($hash = null)
    {
        self::truncate();

        $model = new static();
        $model->setHash($hash);
        $model->save();
    }

    private function setHash($hash = null)
    {
        $this->hash = $hash ?: hash_hmac('sha256', Str::random(32), config('app.key'));
    }

    public function save($options = [])
    {
        self::truncate();

        parent::save($options);
    }
}
