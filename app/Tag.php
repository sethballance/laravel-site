<?php

namespace App;

use App\Project;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_tags')->withTimestamps();
    }
}
