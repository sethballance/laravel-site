<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AuthToken;

class CreateAuthToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:createToken {hash?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an authorization token to be added to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hash = $this->argument('hash');

        AuthToken::reset($hash);

        $this->info('Authorization token created');
    }
}
