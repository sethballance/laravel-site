<?php

namespace App;

use App\Tag;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'title',
        'description',
        'project_link'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'project_tags')->orderBy('name', 'asc')->withTimestamps();
    }
}
