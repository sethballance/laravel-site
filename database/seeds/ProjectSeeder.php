<?php

use App\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Seed the projects table.
     *
     * @return void
     */
    public function run()
    {
        factory(Project::class, 5)->create();
    }
}
