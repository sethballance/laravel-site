<?php

use App\Tag;
use App\Project;
use App\AuthToken;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call([ProjectSeeder::class, TagSeeder::class]);

        $tags = Tag::all();

        Project::all()->each(function ($project) use ($tags) {
            $project->tags()->attach(
                $tags->random(rand(1, 3))->pluck('id')->toArray()
            );
        });

        AuthToken::reset();
    }
}
