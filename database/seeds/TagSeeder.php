<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Seed the tags table.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class, 15)->create();
    }
}
